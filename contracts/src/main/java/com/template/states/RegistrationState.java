package com.template.states;

import com.google.common.collect.ImmutableList;
import com.template.contracts.RegistrationContract;
import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import net.corda.core.serialization.ConstructorForDeserialization;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@BelongsToContract(RegistrationContract.class)
public class RegistrationState implements LinearState {
    public static final String LEGAL_TYPE = "LEGAL", NATURAL_TYPE = "NATURAL";

    private final String alias; /* The name its known by. Not unique: "EAFIT", "Alice"... */
    private final String type; /* "NATURAL", "LEGAL" */
    private final Party registrar; /* The one that registers the person (SmartID) */
    private final Party registrant; /* Needed in the case of legal */
    private final String cedula;

    private final UniqueIdentifier linearId;

    @ConstructorForDeserialization
    public RegistrationState(String alias, String cedula, String type, Party registrar, Party registrant, UniqueIdentifier linearId) {
        this.alias = alias;
        this.cedula = cedula;
        this.type = type;
        this.registrar = registrar;
        this.registrant = registrant;
        this.linearId = linearId;
    }

    @Deprecated
    public RegistrationState(String alias, String type, Party registrar, Party registrant) {
        this.alias = alias;
        this.cedula = null;
        this.type = type;
        this.registrar = registrar;
        this.registrant = registrant;
        this.linearId = new UniqueIdentifier();
    }

    public RegistrationState(String alias, String cedula, String type, Party registrar, Party registrant) {
        this.alias = alias;
        this.cedula = cedula;
        this.type = type;
        this.registrar = registrar;
        this.registrant = registrant;
        this.linearId = new UniqueIdentifier();
    }

    public String getAlias() {
        return alias;
    }

    public String getCedula() {
        return cedula;
    }

    public String getType() {
        return type;
    }

    public Party getRegistrar() {
        return registrar;
    }

    public Party getRegistrant() {
        return registrant;
    }

    @NotNull
    @Override
    public UniqueIdentifier getLinearId() {
        return linearId;
    }

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        if(registrant != null) {
            return ImmutableList.of(registrar, registrant);
        } else {
            return ImmutableList.of(registrar);
        }
    }
}
