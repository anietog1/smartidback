package com.template.states;

import com.google.common.collect.ImmutableList;
import com.template.contracts.CertificationContract;
import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import net.corda.core.serialization.ConstructorForDeserialization;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

@BelongsToContract(CertificationContract.class)
public class CertificationState implements LinearState {
    public static final String B2P_TYPE = "B2P";

    private final Party certifier; // Legal person
    private final Party certified; // Smart

    private final UUID certifierId;
    private final UUID certifiedId;

    private final String type;
    private final String title;
    private final String description;
    private final String[] tags;
    private final String metadata;

    private final UniqueIdentifier linearId;

    @ConstructorForDeserialization
    public CertificationState(Party certifier, Party certified,
                              UUID certifierId, UUID certifiedId,
                              String type, String title,
                              String description, String[] tags,
                              String metadata,
                              UniqueIdentifier linearId) {
        this.certifier = certifier;
        this.certified = certified;

        this.certifierId = certifierId;
        this.certifiedId = certifiedId;

        this.type = type;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.metadata = metadata;

        this.linearId = linearId;
    }

    public CertificationState(Party certifier, Party certified,
                              UUID certifierId, UUID certifiedId,
                              String type, String title,
                              String description, String[] tags,
                              String metadata) {
        this.certifier = certifier;
        this.certified = certified;

        this.certifierId = certifierId;
        this.certifiedId = certifiedId;

        this.type = type;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.metadata = metadata;

        this.linearId = new UniqueIdentifier();
    }

    @NotNull
    @Override
    public UniqueIdentifier getLinearId() {
        return linearId;
    }

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        return ImmutableList.of(certifier, certified);
    }

    public Party getCertifier() {
        return certifier;
    }

    public Party getCertified() {
        return certified;
    }

    public UUID getCertifierId() {
        return certifierId;
    }

    public UUID getCertifiedId() {
        return certifiedId;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String[] getTags() {
        return tags;
    }

    public String getMetadata() {
        return metadata;
    }
}
