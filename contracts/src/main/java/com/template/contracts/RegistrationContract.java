package com.template.contracts;

import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.Contract;
import net.corda.core.transactions.LedgerTransaction;
import org.jetbrains.annotations.NotNull;

public class RegistrationContract implements Contract {
    public static String ID = "com.template.contracts.RegistrationContract";

    @Override
    public void verify(@NotNull LedgerTransaction tx) throws IllegalArgumentException {
        // pass
    }

    public interface Commands extends CommandData {
        class RegisterNP implements Commands { } /* Natural Person */
        class RegisterLP implements Commands { } /* Legal Person */
    }
}
