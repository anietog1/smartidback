package com.template.contracts;

import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.Contract;
import net.corda.core.transactions.LedgerTransaction;
import org.jetbrains.annotations.NotNull;

public class CertificationContract implements Contract {
    public static String ID = "com.template.contracts.CertificationContract";

    @Override
    public void verify(@NotNull LedgerTransaction tx) throws IllegalArgumentException {
        // pass
    }

    public interface Commands extends CommandData {
        //class CertifyB2B implements Commands { } /* Business to business */
        class CertifyB2P implements Commands { } /* Business to person */
        //class CertifyP2P implements Commands { } /* Person to person */
        //class CertifyP2B implements Commands { } /* Person to business */
    }
}
