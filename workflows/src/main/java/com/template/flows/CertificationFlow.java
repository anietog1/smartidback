package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableList;
import com.template.contracts.CertificationContract;
import com.template.states.CertificationState;
import com.template.states.RegistrationState;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.crypto.SecureHash;
import net.corda.core.flows.*;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import org.jetbrains.annotations.NotNull;
import net.corda.core.utilities.ProgressTracker.Step;

import java.util.UUID;

import static net.corda.core.contracts.ContractsDSL.requireThat;

public class CertificationFlow {
    @InitiatingFlow
    @StartableByRPC
    public static class Initiator extends FlowLogic<UniqueIdentifier> {
        private final Party certified;
        private final UUID certifiedId;

        private final String type;
        private final String title;
        private final String description;
        private final String[] tags;
        private final String metadata;

        private final Step GENERATING_TRANSACTION = new Step("Generating registration transaction.");
        private final Step VERIFYING_TRANSACTION = new Step("Verifying contract constraints.");
        private final Step SIGNING_TRANSACTION = new Step("Signing transaction with our private key.");
        private final Step GATHERING_SIGNATURES = new Step("Gathering the signatures.") {
            @Override
            public ProgressTracker childProgressTracker() {
                return CollectSignaturesFlow.Companion.tracker();
            }
        };
        private final Step FINALISING_TRANSACTION = new Step("Obtaining notary signature and recording transaction.") {
            @Override
            public ProgressTracker childProgressTracker() {
                return FinalityFlow.Companion.tracker();
            }
        };

        private final ProgressTracker progressTracker = new ProgressTracker(
                GENERATING_TRANSACTION,
                VERIFYING_TRANSACTION,
                SIGNING_TRANSACTION,
                GATHERING_SIGNATURES,
                FINALISING_TRANSACTION
        );

        public Initiator(Party certified, String certifiedId,
                         String title, String description,
                         String[] tags, String metadata) {
            this.certified = certified;
            this.certifiedId = UUID.fromString(certifiedId);
            this.type = CertificationState.B2P_TYPE;
            this.title = title;
            this.description = description;
            this.tags = tags;
            this.metadata = metadata;
        }

        @Suspendable
        @Override
        public UniqueIdentifier call() throws FlowException {
            Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);

            progressTracker.setCurrentStep(GENERATING_TRANSACTION);

            Party certifier = getOurIdentity();
            UUID certifierId = getServiceHub().getVaultService()
                    .queryBy(RegistrationState.class).getStates().get(0)
                    .getState().getData().getLinearId().getId();

            CertificationState certificationState =
                    new CertificationState(certifier, certified, certifierId, certifiedId, type, title, description, tags, metadata);

            Command command = new Command(new CertificationContract.Commands.CertifyB2P(),
                    ImmutableList.of(certifier.getOwningKey(), certified.getOwningKey()));

            TransactionBuilder txBuilder = new TransactionBuilder(notary)
                    .addOutputState(certificationState, CertificationContract.ID)
                    .addCommand(command);

            progressTracker.setCurrentStep(VERIFYING_TRANSACTION);
            txBuilder.verify(getServiceHub());

            progressTracker.setCurrentStep(SIGNING_TRANSACTION);
            SignedTransaction signedTx = getServiceHub().signInitialTransaction(txBuilder);

            progressTracker.setCurrentStep(GATHERING_SIGNATURES);
            FlowSession counterpartySession = initiateFlow(certified);
            signedTx = subFlow(new CollectSignaturesFlow(signedTx, ImmutableList.of(counterpartySession)));

            progressTracker.setCurrentStep(FINALISING_TRANSACTION);
            signedTx = subFlow(new FinalityFlow(signedTx, ImmutableList.of(counterpartySession)));

            return signedTx.getTx().outputsOfType(CertificationState.class).get(0).getLinearId();
        }

        @Override
        public ProgressTracker getProgressTracker() {
            return progressTracker;
        }
    }

    @InitiatedBy(Initiator.class)
    public static class Responder extends FlowLogic<SignedTransaction> {
        private FlowSession counterpartySession;

        public Responder(FlowSession counterpartySession) {
            this.counterpartySession = counterpartySession;
        }

        @Suspendable
        @Override
        public SignedTransaction call() throws FlowException {
            SignTransactionFlow signTransactionFlow = new SignTransactionFlow(counterpartySession) {
                @Override
                protected void checkTransaction(@NotNull SignedTransaction stx) throws FlowException {
                    requireThat(require -> {
                        return null;
                    });
                }
            };

            SecureHash txId = subFlow(signTransactionFlow).getId();
            return subFlow(new ReceiveFinalityFlow(counterpartySession, txId));
        }
    }
}
