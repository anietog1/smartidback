package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableList;
import com.template.contracts.RegistrationContract;
import com.template.states.RegistrationState;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.crypto.SecureHash;
import net.corda.core.flows.*;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import net.corda.core.utilities.ProgressTracker.Step;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static net.corda.core.contracts.ContractsDSL.requireThat;

public class RegistrationFlow {
    @InitiatingFlow
    @StartableByRPC
    public static class Initiator extends FlowLogic<UniqueIdentifier> {
        private final Boolean isRegistrar;
        private final Party counterparty;
        private final String alias;
        private final String cedula;

        private final Step GENERATING_TRANSACTION = new Step("Generating registration transaction.");
        private final Step VERIFYING_TRANSACTION = new Step("Verifying contract constraints.");
        private final Step SIGNING_TRANSACTION = new Step("Signing transaction with our private key.");
        private final Step GATHERING_SIGNATURES = new Step("Gathering the signatures.") {
            @Override
            public ProgressTracker childProgressTracker() {
                return CollectSignaturesFlow.Companion.tracker();
            }
        };
        private final Step FINALISING_TRANSACTION = new Step("Obtaining notary signature and recording transaction.") {
            @Override
            public ProgressTracker childProgressTracker() {
                return FinalityFlow.Companion.tracker();
            }
        };

        private final ProgressTracker progressTracker = new ProgressTracker(
                GENERATING_TRANSACTION,
                VERIFYING_TRANSACTION,
                SIGNING_TRANSACTION,
                GATHERING_SIGNATURES,
                FINALISING_TRANSACTION
        );

        public Initiator(Party counterparty, String alias) {
            this.isRegistrar = false;
            this.counterparty = counterparty;
            this.alias = alias;
            this.cedula = null;
        }

        @Deprecated
        public Initiator(String alias) {
            this.isRegistrar = true;
            this.counterparty = null;
            this.alias = alias;
            this.cedula = null;
        }

        public Initiator(String alias, String cedula) {
            this.isRegistrar = true;
            this.counterparty = null;
            this.alias = alias;
            this.cedula = cedula;
        }

        @Override
        public ProgressTracker getProgressTracker() {
            return progressTracker;
        }

        @Suspendable
        @Override
        public UniqueIdentifier call() throws FlowException {
            Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);

            progressTracker.setCurrentStep(GENERATING_TRANSACTION);
            Party registrant, registrar;
            String type;

            if (isRegistrar) {
                registrar = getOurIdentity();
                registrant = counterparty;

                if(registrant != null) {
                    type = RegistrationState.LEGAL_TYPE;
                } else {
                    type = RegistrationState.NATURAL_TYPE;
                }
            } else {
                registrar = counterparty;
                registrant = getOurIdentity();
                type = RegistrationState.LEGAL_TYPE;
            }

            RegistrationState registrationState = new RegistrationState(alias, cedula, type, registrar, registrant);
            Command command;

            if(type == RegistrationState.LEGAL_TYPE) {
                command = new Command(new RegistrationContract.Commands.RegisterLP(), ImmutableList.of(registrar.getOwningKey(), registrant.getOwningKey()));
            } else {
                command = new Command(new RegistrationContract.Commands.RegisterNP(), ImmutableList.of(registrar.getOwningKey()));
            }

            TransactionBuilder txBuilder = new TransactionBuilder(notary)
                    .addOutputState(registrationState, RegistrationContract.ID)
                    .addCommand(command);

            progressTracker.setCurrentStep(VERIFYING_TRANSACTION);
            txBuilder.verify(getServiceHub());

            progressTracker.setCurrentStep(SIGNING_TRANSACTION);
            SignedTransaction signedTx = getServiceHub().signInitialTransaction(txBuilder);

            if(type == RegistrationState.LEGAL_TYPE) {
                progressTracker.setCurrentStep(GATHERING_SIGNATURES);
                FlowSession counterpartySession = initiateFlow(counterparty);
                signedTx = subFlow(new CollectSignaturesFlow(signedTx, ImmutableList.of(counterpartySession)));


                progressTracker.setCurrentStep(FINALISING_TRANSACTION);
                signedTx = subFlow(new FinalityFlow(signedTx, ImmutableList.of(counterpartySession)));
            } else {
                progressTracker.setCurrentStep(FINALISING_TRANSACTION);
                signedTx = subFlow(new FinalityFlow(signedTx));
            }

            return signedTx.getTx().outputsOfType(RegistrationState.class).get(0).getLinearId();
        }
    }

    @InitiatedBy(Initiator.class)
    public static class Responder extends FlowLogic<SignedTransaction> {
        private FlowSession counterpartySession;

        public Responder(FlowSession counterpartySession) {
            this.counterpartySession = counterpartySession;
        }

        @Suspendable
        @Override
        public SignedTransaction call() throws FlowException {
            SignTransactionFlow signTransactionFlow = new SignTransactionFlow(counterpartySession) {
                @Override
                protected void checkTransaction(@NotNull SignedTransaction stx) throws FlowException {
                    requireThat(require -> {
                        return null;
                    });
                }
            };

            SecureHash txId = subFlow(signTransactionFlow).getId();
            return subFlow(new ReceiveFinalityFlow(counterpartySession, txId));
        }
    }
}
