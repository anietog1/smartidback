package com.template.dto;

import java.util.UUID;

public class CertificateDto {
	private String certifier;
    private UUID certifiedId;
    private String title;
    private String description;
    private String[] tags;
	private String metadata;
    
	public CertificateDto(String certifier, UUID certifiedId, String title, String description, String[] tags,
			String metadata) {
		this.certifier = certifier;
		this.certifiedId = certifiedId;
		this.title = title;
		this.description = description;
		this.tags = tags;
		this.metadata = metadata;
	}
	
	public String getCertifier() {
		return certifier;
	}
	public void setCertifier(String certifier) {
		this.certifier = certifier;
	}
	public UUID getCertifiedId() {
		return certifiedId;
	}
	public void setCertifiedId(UUID certifiedId) {
		this.certifiedId = certifiedId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String[] getTags() {
		return tags;
	}
	public void setTags(String[] tags) {
		this.tags = tags;
	}
	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}	
}
