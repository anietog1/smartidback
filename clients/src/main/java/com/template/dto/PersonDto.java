package com.template.dto;

import java.util.UUID;

import net.corda.core.contracts.UniqueIdentifier;

public class PersonDto {
	private UUID id; // Part of linear Id
	private String alias;
	private String type; /* "NATURAL", "LEGAL" */

	public PersonDto() {
	}

	public PersonDto(String alias) {
		this.alias = alias;
	}

	public PersonDto(String alias, String type, UniqueIdentifier linearId) {
		this.alias = alias;
		this.type = type;
		this.id = linearId.getId();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
