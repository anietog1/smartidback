package com.template.webserver;

import net.corda.core.messaging.CordaRPCOps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.template.dto.CertificateDto;
import com.template.dto.PersonDto;
import com.template.flows.RegistrationFlow;
import com.template.states.CertificationState;
import com.template.states.RegistrationState;

import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.corda.core.contracts.StateAndRef;

@CrossOrigin
@RestController
public class Controller {
	private final CordaRPCOps proxy;
	private final static Logger logger = LoggerFactory.getLogger(Controller.class);

	public Controller(NodeRPCConnection rpc) {
		this.proxy = rpc.proxy;
	}

	@GetMapping(value = "/identities", produces = TEXT_PLAIN_VALUE)
	private String identities() {
		return proxy.nodeInfo().getLegalIdentities().toString();
	}

	@GetMapping(value = "/addresses", produces = "text/plain")
	private String addresses() {
		return proxy.nodeInfo().getAddresses().toString();
	}

	@GetMapping(value = "/flows", produces = TEXT_PLAIN_VALUE)
	private String flows() {
		return proxy.registeredFlows().toString();
	}

	@GetMapping(value = "/row-certificates", produces = { "application/json" })
	private List<StateAndRef<CertificationState>> getRowCertificates() {
		List<StateAndRef<CertificationState>> resultsList = proxy.vaultQuery(CertificationState.class).getStates();
		return resultsList;
	}

	@GetMapping(value = "/persons", produces = { "application/json" })
	private List<PersonDto> getPersons() {
		List<PersonDto> persons = new ArrayList<>();
		List<StateAndRef<RegistrationState>> resultsList = proxy.vaultQuery(RegistrationState.class).getStates();
		for (StateAndRef<RegistrationState> result : resultsList) {
			RegistrationState state = result.getState().getData();
			persons.add(new PersonDto(state.getAlias(), state.getType(), state.getLinearId()));
		}
		return persons;
	}

	private String getLegalPersonAlias(UUID legalPersonId) {
		List<StateAndRef<RegistrationState>> resultsList = proxy.vaultQuery(RegistrationState.class).getStates();
		for (StateAndRef<RegistrationState> result : resultsList) {
			RegistrationState state = result.getState().getData();
			if (state.getLinearId().getId().equals(legalPersonId)) {
				return state.getAlias();
			}
		}
		return null;
	}

	@GetMapping(value = "/certificates", produces = { "application/json" })
	private List<CertificateDto> getCertificates() {
		List<CertificateDto> certificates = new ArrayList<>();
		List<StateAndRef<CertificationState>> resultsList = proxy.vaultQuery(CertificationState.class).getStates();
		for (StateAndRef<CertificationState> result : resultsList) {
			CertificationState state = result.getState().getData();
			String legalPersonName = getLegalPersonAlias(state.getCertifierId());
			certificates.add(new CertificateDto(legalPersonName, state.getCertifiedId(), state.getTitle(),
					state.getDescription(), state.getTags(), state.getMetadata()));
		}
		return certificates;
	}

	@GetMapping(value = "/persons/{id}/certificates", produces = { "application/json" })
	private List<CertificateDto> getPersonCertificates(@PathVariable UUID id) {
		List<CertificateDto> certificates = new ArrayList<>();
		List<StateAndRef<CertificationState>> resultsList = proxy.vaultQuery(CertificationState.class).getStates();
		for (StateAndRef<CertificationState> result : resultsList) {
			CertificationState state = result.getState().getData();
			if (state.getCertifiedId().equals(id)) {
				String legalPersonName = getLegalPersonAlias(state.getCertifierId());
				certificates.add(new CertificateDto(legalPersonName, state.getCertifiedId(), state.getTitle(),
						state.getDescription(), state.getTags(), state.getMetadata()));
			}
		}
		return certificates;
	}

	@GetMapping(value = "/persons/{id}", produces = { "application/json" })
	private ResponseEntity<PersonDto> getPerson(@PathVariable UUID id) {
		PersonDto registry;
		List<StateAndRef<RegistrationState>> resultsList = proxy.vaultQuery(RegistrationState.class).getStates();
		for (StateAndRef<RegistrationState> result : resultsList) {
			RegistrationState state = result.getState().getData();
			if (state.getLinearId().getId().equals(id)) {
				registry = new PersonDto(state.getAlias(), state.getType(), state.getLinearId());
				return new ResponseEntity<PersonDto>(registry, HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping("/persons")
	public ResponseEntity<UUID> registry(@RequestBody PersonDto natural) {
		UUID result;
		try {
			result = proxy.startTrackedFlowDynamic(RegistrationFlow.Initiator.class, natural.getAlias())
					.getReturnValue().get().getId();
		} catch (Throwable ex) {
			// final String msg = ex.getMessage();
			logger.error(ex.getMessage(), ex);

			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<UUID>(result, HttpStatus.OK);
	}
}